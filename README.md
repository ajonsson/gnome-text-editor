# GNOME Text Editor

Text Editor is a simple text editor that focus on session management.
It works hard to keep track of changes and state even if you quit the application.
You can come back to your work even if you've never saved it to a file.

## Dependencies

Text Editor was built as a test application for the development of GTK 4 and GtkSourceView 5 and therefore requires both of those along with libadwaita.
Enchant 2 is used to provide spellchecking capabilities.

A full list of dependencies beyond what is provided by the GNOME SDK can be found in our Flatpak build manifest at [org.gnome.TextEditor.Devel.json](https://gitlab.gnome.org/GNOME/gnome-text-editor/tree/master/org.gnome.TextEditor.Devel.json).

## Plans

Everyone has their favorite programming text editors.
Text Editor is focused on being a great default text editor.

## Try it Out!

You can install our Nightly build from Flatpak using [org.gnome.TextEditor.Devel.flatpakref](https://nightly.gnome.org/repo/appstream/org.gnome.TextEditor.Devel.flatpakref).
