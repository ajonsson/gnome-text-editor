# Text Editor 41.alpha1

Text Editor 41.alpha1 contains a number of new features and bug
fixes since our previous release.

 • Initial support for spell checking
 • Tabs provided by libadwaita
 • Improved performance while loading documents
 • Style improvements to overview map, line spacing, search,
   status pages, open popover, and more.
 • Additional settings in preferences
 • Improved auto-save support
 • A new buffer scheduler to improve performance of background
   operations.
 • Support for opening multiple files at once
 • Translation improvements

# Text Editor 40.0

Text Editor for GNOME 40 is here!

This is just a preview release but kick the tires and have some fun!


# Text Editor 3.39.92

This is my initial release of gnome-text-editor.

It started as an application for me to verify the correctness of the
GtkSourceView 5 API (which targets GTK 4). After that it helped me implement
JIT support for GtkSourceView languages. Once that was done it became my
test case while I wrote the GTK 4 macOS backend and revamped the GL renderer.

It is a simple and humble text editor. It does not have all the corner cases
you'd expect from a text editor yet.

Now that you know this is very much a technology preview release only, you
might be tempted to keep your safe data away from it.

## What it can do

 • Simple interface designed by the GNOME design team. You can find the
   mockups in the traditional places such as:
   https://gitlab.gnome.org/Teams/Design/app-mockups
 • Search and Replace
 • Typical GtkSourceView features
 • Quick access to recent documents
 • Multiple windows
 • Automatic discovery of .editorconfig and modelines settings
 • Light and dark mode
 • Automatically save files to drafts, restored in case of crash
 • Can be run from within a Flatpak sandbox and uses document portal
   for access to host files.

## What it cannot do

 • It doesn't protect you from trying to open really large files
 • Support your custom GTK 4 theme
 • Auto-completion or snippets
 • Plugins
 • Custom file encodings

## Building

If you'd like to test it out, the easiest way is to clone the repository
from GNOME Builder and click Run. Additionally, you can find a Flatpak in
the gnome-nightly¹ Flatpak repository.

¹ https://wiki.gnome.org/Apps/Nightly
